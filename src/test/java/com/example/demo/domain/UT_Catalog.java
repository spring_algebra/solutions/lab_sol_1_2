/*
 * Algebra labs.
 */
package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.example.demo.service.Catalog;

public class UT_Catalog {

    @Test
    public void catalogTest() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        Assert.assertTrue("spring container should not be null", ctx != null);

        // Look up catalog, make sure it is non-null and output it
        Catalog cat = ctx.getBean(Catalog.class);
        Assert.assertTrue("Catalog should be non-null", cat != null);
        System.out.println(cat);

        ctx.close();
    }

}
