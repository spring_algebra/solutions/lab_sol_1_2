/*
 * Algebra labs.
 */

package com.example.demo.persistence;

import java.util.Collection;

import com.example.demo.domain.MusicItem;

public interface ItemRepository {

  public com.example.demo.domain.MusicItem get(Long id);

  public Collection<MusicItem> getAll();

  public Collection<MusicItem> searchByArtistTitle(String keyword);
  
  public int size();
}
